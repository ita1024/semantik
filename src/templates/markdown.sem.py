#sem:name: Markdown
#sem:tip: Generates a simple Markdown document (experimental)

# Thomas Nagy, 2023 GPLV3

import getpass, shutil

outdir = sembind.get_var('outdir')+'/'+sembind.get_var('pname')

settings = {
'doc_content':'',
'doc_title':'',
'doc_author': getpass.getuser(),
}
add_globals(settings)

temp_dir = sembind.get_var('temp_dir')
pics = index_pictures(outdir)

buf = []
out = buf.append

def p(s):
	return sembind.protectHTML(s)

def xml(s):
	return sembind.protectXML(s)

def print_nodes(node, niv, lbl_lst):

	lbl = ".".join(lbl_lst)

	caption = node.get_var('caption')
	if not caption and not node.get_var('disable_caption', False):
		caption = node.get_val('summary')

	typo = node.get_val('type')
	if typo in ['text']:
		if niv in range(5):
			sep = ''
			if lbl:
				sep = ' '
			out('%s%s%s %s\n\n' % ((1 + niv) * '#', sep, lbl, node.get_val('summary')))

		body = parse_raw(node.get_val('text')).strip()
		lang = node.get_var('code_lang').strip()
		if body:
			if lang:
				out('```%s\n%s```' % (lang, body))
				out('\n\n')
			else:
				out(body)
				out('\n\n')

	elif typo == 'table':
		rows = node.num_rows()
		cols = node.num_cols()


		if xml(caption):
			out('%s\n\n' % caption)

		row_sizes = {}
		col_sizes = {}
		if rows > 0 and cols > 0:
			for i in range(rows):
				row_sizes[i] = 0
				for j in range(cols):
					cell = xml(node.get_cell(i, j)).rstrip()
					lines = cell.splitlines()
					for line in lines:
						row_sizes[i] = max(row_sizes.get(i, 0), len(lines))
						col_sizes[j] = max(col_sizes.get(j, 0), len(line))

			out('\n')
			for i in range(rows):
				if i > 0:
					out('|')
					for j in range(cols):
						out('-' * col_sizes[j])
						out('|')
					out('\n')

				for k in range(row_sizes[i]):
					out('|')
					for j in range(cols):
						cell = xml(node.get_cell(i, j)).rstrip()
						colsize = col_sizes[j]

						try:
							line = cell.splitlines()[k]
							line = line.strip().ljust(colsize)
						except IndexError:
							line = ''.ljust(colsize)

						out(line)
						out('|')
					out('\n')
		out('\n')

	elif typo == 'img' or typo == 'diag':
		the_pic = pics.get(node.get_val('id'))
		if the_pic and not node.get_var('exclude_pic'):
			out('\n![%s](%s)\n\n' % (xml(caption), the_pic))

	num = node.child_count()
	for i in range(num):
		print_nodes(node.child_num(i), niv+1, lbl_lst+[str(i+1)])

# the main document
print_nodes(Root(), 0, []);
settings['doc_content'] = "".join(buf)

transform("/markdown/doc.md", outdir+'/doc.md', settings)

